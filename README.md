# Developments process

First of all, you should do the following steps:

1. Clone the repo
2. Install [node](https://nodejs.org/en/download/)
3. Run `npm install`
4. Get an account in [firebase](https://firebase.io)
5. (Optional) Set up firebase cli (only if you need it, our deploy pipeline works in gitlab ci/cd just fine): `npm install -g firebase-tools`
6. (Optional) Login to firebase: `firebase login`
7. When done run `git commit`. Our commits are based on commitizen, so it will format your commit message for you.
