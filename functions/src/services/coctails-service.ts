import { FirebaseError } from 'firebase-admin';
import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';

import { firestore } from '../configs/db';

const cocktailsCollection = firestore.collection('cocktails');

const app = express();
app.use(cors({ origin: true }));

app.get('/', async (req, res) => {
  try {
    const campaignsData = await cocktailsCollection
      .get()
      .then((querySnapshot) => {
        return querySnapshot.docs.map((doc) => doc.data());
      });

    res.send(campaignsData);
  } catch (error) {
    res.send(new Error(JSON.stringify(error as FirebaseError)));
  }
});

export const cocktails = functions.https.onRequest(app);
