class CocktailsApiService {
  constructor() {
    this.entrypoint =
      'https://us-central1-concoction-8c65f.cloudfunctions.net/cocktails';
  }

  async getAllCocktails() {
    const coctails = await fetch(this.entrypoint).then((r) => r.json());

    return coctails;
  }
}
